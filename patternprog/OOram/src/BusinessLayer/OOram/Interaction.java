package BusinessLayer.OOram;

public class Interaction 
{
    private Method method;    
    private Role sourceRole;
    private Port throughPort;

    /**
     * @return the method
     */
    public Method getMethod() 
    {
        return method;
    }

    /**
     * @param method the method to set
     */
    public void setMethod(Method method) 
    {
        this.method = method;
    }

    /**
     * @return the throughPort
     */
    public Port getThroughPort() 
    {
        return throughPort;
    }

    /**
     * @param throughPort the throughPort to set
     */
    public void setThroughPort(Port throughPort) 
    {
        this.throughPort = throughPort;
    }

    /**
     * @return the sourceRole
     */
    public Role getSourceRole() 
    {
        return sourceRole;
    }

    /**
     * @param sourceRole the sourceRole to set
     */
    public void setSourceRole(Role sourceRole) 
    {
        this.sourceRole = sourceRole;
    }
    
    
}
