package BusinessLayer.OOram;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class RoleModel 
{
    private String name;
    private ScenarioMethod templateMethod;
    private Set<HookMethod> hookMethods= new LinkedHashSet<>();
    private Set<Role> roles= new LinkedHashSet<>();

    /**
     * @return the name
     */
    public String getName() 
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) 
    {
        this.name = name;
    }

    /**
     * @return the templateMethod
     */
    public ScenarioMethod getTemplateMethod() 
    {
        return templateMethod;
    }

    /**
     * @param templateMethod the templateMethod to set
     */
    public void setTemplateMethod(ScenarioMethod templateMethod) 
    {
        this.templateMethod = templateMethod;
    }

    /**
     * @return the hookMethods
     */
    public Set<HookMethod> getHookMethods() 
    {
        return hookMethods;
    }

    /**
     * @param hookMethods the hookMethods to set
     */
    public void setHookMethods(Set<HookMethod> hookMethods) 
    {
        this.hookMethods = hookMethods;
    }

    /**
     * @return the roles
     */
    public Set<Role> getRoles() 
    {
        return roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(Set<Role> roles) 
    {
        this.roles = roles;
    }

    @Override
    public int hashCode() 
    {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if (obj == null) 
        {
            return false;
        }
        if (getClass() != obj.getClass()) 
        {
            return false;
        }
        final RoleModel other = (RoleModel) obj;
        if (!Objects.equals(this.name, other.name)) 
        {
            return false;
        }
        return true;
    }
    
    
}
