package BusinessLayer.OOram;

import java.util.LinkedHashSet;
import java.util.Set;

public class ProjectModule 
{
    private String name;
    private Set <RoleModel> roleModels= new LinkedHashSet<>();
    private Set <RoleLink> roleLinks= new LinkedHashSet<>();
    
    
    /**
     * @return the name
     */
    public String getName() 
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) 
    {
        this.name = name;
    }

    /**
     * @return the roleModels
     */
    public Set <RoleModel> getRoleModels() 
    {
        return roleModels;
    }

    /**
     * @param roleModels the roleModels to set
     */
    public void setRoleModels(Set <RoleModel> roleModels) 
    {
        this.roleModels = roleModels;
    }

    /**
     * @return the roleLinks
     */
    public Set <RoleLink> getRoleLinks() 
    {
        return roleLinks;
    }

    /**
     * @param roleLinks the roleLinks to set
     */
    public void setRoleLinks(Set <RoleLink> roleLinks) 
    {
        this.roleLinks = roleLinks;
    }
    
}
