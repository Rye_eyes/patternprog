package BusinessLayer.OOram;

import java.util.Objects;

public class Interface 
{
    private Role sourceRole;
    private Role destinationRole;    

    @Override
    public int hashCode() 
    {
        int hash = 3;
        hash = 13 * hash + Objects.hashCode(this.sourceRole);
        hash = 13 * hash + Objects.hashCode(this.destinationRole);
        return hash;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if (obj == null) 
        {
            return false;
        }
        
        if (getClass() != obj.getClass()) 
        {
            return false;
        }
        
        final Interface other = (Interface) obj;
        
        if (!Objects.equals(this.sourceRole, other.sourceRole)) 
        {
            return false;
        }
        
        if (!Objects.equals(this.destinationRole, other.destinationRole)) 
        {
            return false;
        }
        return true;
    }

    
    /**
     * @return the sourceRole
     */
    public Role getSourceRole() 
    {
        return sourceRole;
    }

    /**
     * @param sourceRole the sourceRole to set
     */
    public void setSourceRole(Role sourceRole) 
    {
        this.sourceRole = sourceRole;
    }

    /**
     * @return the destinationRole
     */
    public Role getDestinationRole() 
    {
        return destinationRole;
    }

    /**
     * @param destinationRole the destinationRole to set
     */
    public void setDestinationRole(Role destinationRole) 
    {
        this.destinationRole = destinationRole;
    }
}
