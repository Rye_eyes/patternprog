package BusinessLayer.OOram;

import java.util.ArrayList;
import java.util.List;

public class ScenarioMethod extends Method
{
   private List<Interaction> orderedInteractions=new ArrayList<>();

    /**
     * @return the orderedInteractions
     */
    public List<Interaction> getOrderedInteractions() 
    {
        return orderedInteractions;
    }

    /**
     * @param orderedInteractions the orderedInteractions to set
     */
    public void setOrderedInteractions(List<Interaction> orderedInteractions) 
    {
        this.orderedInteractions = orderedInteractions;
    }

   
   
}
