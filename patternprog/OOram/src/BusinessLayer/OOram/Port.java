package BusinessLayer.OOram;

import java.util.Objects;

public class Port 
{
    private String name;
    private Boolean isMultiple;
    private Interface interfaceType;

    /**
     * @return the name
     */
    public String getName() 
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) 
    {
        this.name = name;
    }

    /**
     * @return the isMultiple
     */
    public Boolean getIsMultiple() 
    {
        return isMultiple;
    }

    /**
     * @param isMultiple the isMultiple to set
     */
    public void setIsMultiple(Boolean isMultiple) 
    {
        this.isMultiple = isMultiple;
    }

    /**
     * @return the interfaceType
     */
    public Interface getInterfaceType() 
    {
        return interfaceType;
    }

    /**
     * @param interfaceType the interfaceType to set
     */
    public void setInterfaceType(Interface interfaceType) 
    {
        this.interfaceType = interfaceType;
    }

    @Override
    public int hashCode() 
    {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if (obj == null) 
        {
            return false;
        }
        if (getClass() != obj.getClass()) 
        {
            return false;
        }
        final Port other = (Port) obj;
        if (!Objects.equals(this.name, other.name)) 
        {
            return false;
        }
        return true;
    }

   
        
    
}
