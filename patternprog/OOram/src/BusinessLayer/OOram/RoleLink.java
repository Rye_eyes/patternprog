package BusinessLayer.OOram;

import java.util.ArrayList;
import java.util.List;

public abstract class RoleLink 
{
    private List<Role> rolesImposed= new ArrayList<>();

    /**
     * @return the rolesImposed
     */
    public List<Role> getRolesImposed() 
    {
        return rolesImposed;
    }

    /**
     * @param rolesImposed the rolesImposed to set
     */
    public void setRolesImposed(List<Role> rolesImposed) 
    {
        this.rolesImposed = rolesImposed;
    }
    
}
