package BusinessLayer.OOram;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;


public class Method 
{
    private String name;
    private String returnType="void";
    private Set<Attribute> parameters=new LinkedHashSet<>();
    private Interface owningInterface;

    /**
     * @return the name
     */
    public String getName() 
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) 
    {
        this.name = name;
    }

    /**
     * @return the returnType
     */
    public String getReturnType() 
    {
        return returnType;
    }

    /**
     * @param returnType the returnType to set
     */
    public void setReturnType(String returnType) 
    {
        this.returnType = returnType;
    }

    /**
     * @return the parameters
     */
    public Set<Attribute> getParameters() 
    {
        return parameters;
    }

    /**
     * @param parameters the parameters to set
     */
    public void setParameters(Set<Attribute> parameters) 
    {
        this.parameters = parameters;
    }

    /**
     * @return the owningInterface
     */
    public Interface getOwningInterface() 
    {
        return owningInterface;
    }

    /**
     * @param owningInterface the owningInterface to set
     */
    public void setOwningInterface(Interface owningInterface) 
    {
        this.owningInterface = owningInterface;
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.name);
        hash = 29 * hash + Objects.hashCode(this.returnType);
        hash = 29 * hash + Objects.hashCode(this.parameters);
        hash = 29 * hash + Objects.hashCode(this.owningInterface);
        return hash;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if (obj == null) 
        {
            return false;
        }
        
        if (getClass() != obj.getClass()) 
        {
            return false;
        }
        
        final Method other = (Method) obj;
        if (!Objects.equals(this.name, other.name)) 
        {
            return false;
        }
        
        if (!Objects.equals(this.returnType, other.returnType)) 
        {
            return false;
        }
        
        if (!Objects.equals(this.parameters, other.parameters)) 
        {
            return false;
        }
        
        if (!Objects.equals(this.owningInterface, other.owningInterface)) 
        {
            return false;
        }
        return true;
    }
 
    
    
}
