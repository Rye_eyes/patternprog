package BusinessLayer.OOram;

public class DirectedRoleLink extends RoleLink
{
    private Role destinationRole;
    private LinkType type;

    /**
     * @return the destinationRole
     */
    public Role getDestinationRole() 
    {
        return destinationRole;
    }

    /**
     * @param destinationRole the destinationRole to set
     */
    public void setDestinationRole(Role destinationRole) 
    {
        this.destinationRole = destinationRole;
    }

    /**
     * @return the type
     */
    public LinkType getType() 
    {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(LinkType type) 
    {
        this.type = type;
    }
    
}
