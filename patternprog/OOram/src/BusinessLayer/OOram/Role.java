package BusinessLayer.OOram;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Role 
{
    private String name;
    private Set<Port> ports=new LinkedHashSet<>();

    /**
     * @return the name
     */
    public String getName() 
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) 
    {
        this.name = name;
    }

    /**
     * @return the ports
     */
    public Set<Port> getPorts() 
    {
        return ports;
    }

    /**
     * @param ports the ports to set
     */
    public void setPorts(Set<Port> ports) 
    {
        this.ports = ports;
    }

    @Override
    public int hashCode() 
    {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if (obj == null) 
        {
            return false;
        }
        
        if (getClass() != obj.getClass()) 
        {
            return false;
        }
        final Role other = (Role) obj;
        if (!Objects.equals(this.name, other.name)) 
        {
            return false;
        }
        return true;
    }
    
    

}
