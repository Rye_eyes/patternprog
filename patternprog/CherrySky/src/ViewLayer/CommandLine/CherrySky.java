//package ViewLayer.CommandLine;
//
//import BusinessLayer.OOram.ProjectModule;
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.FileWriter;
//import java.io.InputStreamReader;
//import java.io.PrintWriter;
//
//
///*Главный класс средства конструирования фреймворков на основе паттернов
// с использованием ролевой модели OOram*/
//
//public class CherrySky 
//{
//    /* Перечисление допустимых состояний программы*/
//    //TODO:  работа с существующей моделью
//    //TODO: работа с метриками
//    enum CommandRecognizingState
//    {
//        ROLE_MODEL_DECLARATION, /*Ввод и конфигурирование отдельных библиотечных паттернов*/
//        ROLE_MODEL_SYNTHESIS,   /*Ввод операций синтеза требований отдельных ролевых моделей паттернов*/
//        ROLE_CLASS_MATCH        /*Ввод сопоставления ролей моделей классам предметной области (инстанциирование фреймворка)*/
//    }
//    //TODO
//    enum Command
//    {
//        load_project,
//        add_model
////    }
//    
//    enum State
//    {
//        PROJECT_NAME_SPECIFYING /*Определение имени файла проекта*/
//    }
//    
//  
//    
//    /*Главный управляющий поток*/
//    
//    public static void main(String[] args) throws Exception 
//    {
//        
//            /* Открываем на чтение загрузочный файл моделей паттернов OOram*/
//        
//            String roleModelLoadModuleName=null;
//            CommandRecognizingState currentCommandState=CommandRecognizingState.ROLE_MODEL_DECLARATION;
//            
//            
//            if (args.length!=1)
//            {
//                throw new Exception("Wrong command line format (OOram main model file must be specified).\n");
//            }
//            
//            roleModelLoadModuleName=args[0];
//            
//            BufferedReader roleModelLoadModuleReader=null;
//            
//            try
//            {
//                roleModelLoadModuleReader= new BufferedReader(new FileReader(roleModelLoadModuleName));
//            }
//            catch (FileNotFoundException e)
//            {
//                throw new Exception("Main OOram model file not found.\n");
//            }
//            
//            
//            /* Ждём консольного ввода команд*/
//            
//             ProjectModule projectModule; 
//            //Parser parser= new Parser();
//           
//            BufferedReader commandReader= new BufferedReader(new InputStreamReader(System.in));
//            PrintWriter projectFileWriter=null;
//            
//            State programCurrentState=State.PROJECT_NAME_SPECIFYING;
//            
//            while (true)
//            {
//                String request=commandReader.readLine();
//                
//                String [] requestSplitted=request.split(" ");
//                
//                if (requestSplitted.length<1)
//                {
//                    //TODO: summary+add exception
//                    throw new Exception("Wrong command line format (command must be specified).\n");
//                }
//                
//                String command=requestSplitted[0];
//                
//                switch (programCurrentState)
//                {
//                    case PROJECT_NAME_SPECIFYING:
//                    {
//                        if (requestSplitted.length<2)
//                        {
//                            throw new Exception("Wrong command line format (project file name must be specified).\n");
//                        };
//                        
//                        String name=requestSplitted[1];
//                        
//                        switch(command)
//                        {
//                            
//                            case "create_project":
//                            {
//                                File projectFile=new File(name);
//                            }
//                            case "load_project":
//                            {
//                                
//                                projectFileWriter= new PrintWriter(new FileWriter(name, true));
//                                projectModule=parser.loadModule(name);
//                            
//                            }
//                            case "stop":
//                            {
//                                System.out.println("Program is successfully stopped.\n");
//                                break;
//                            }
//                            default:
//                            {
//                                throw new Exception("Command not found.\n");
//                            }
//                        };
//                        
//                        
//                    }
//                    default:
//                    {
//                        throw new Exception("Invalid program state.\n");
//                    }
//                        
//                }
//                
//            }
//            
//    }
//    
//}
